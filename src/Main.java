import app.Service;
import model.Contact;
import util.Generator;
import view.MainFrame;


public class Main {

    public static void main(String[] args) {
        System.out.println("전화번호부를 시작합니다...");
        Service svc = new Service();

        // 테스트 데이터 생성
        for (int i = 1; i <= 100; i++) {
            String lastName        = "Doe";
            String firstName       = "John" + i;
            String phoneNumberType = "휴대전화";
            String phoneNumber     = String.format("010-1234-0%03d", i);
            String emailType       = "집";
            String email = String.format("a%04d@abc.com", i);
            // 생성한 데이터를 추가
            svc.addContact(new Contact(lastName, firstName, phoneNumberType, phoneNumber, emailType, email));
        }
        System.out.println("dummy data:" + svc.getContacts().size());

        MainFrame mainFrm = new MainFrame(svc);

        System.out.println("구동 완료");
    }
}
