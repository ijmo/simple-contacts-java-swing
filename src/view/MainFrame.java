package view;

import app.Service;
import model.Contact;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame { // 처음 띄워지는 가장 큰 부모 윈도우
    private Service svc;
    private DetailDialog dialog = new DetailDialog(this);

    // 윗쪽 swing components
    private JLabel searchLabel = new JLabel("검색 :");
    private String[] searchCond = {"이름", "전화번호"};
    private JComboBox searchCondComboBox = new JComboBox(searchCond);
    private JTextField searchWordField = new JTextField(16);
    private JButton resetBtn = new JButton("X");
    private JButton searchBtn = new JButton("찾기");
    private JButton addBtn = new JButton("추가");
    private JButton removeBtn = new JButton("선택삭제");
    private JPanel topPanel = new JPanel();

    // 중간 swing components
    private ContactsTable table;
    private JScrollPane scrollPane;

    // 아래쪽 swing components
    private JLabel statusLabel = new JLabel(" ");
    private JPanel bottomPanel = new JPanel();


    public MainFrame(Service svc) {
        this.setTitle("전화번호부");
        this.svc = svc;
        this.table = new ContactsTable(this, svc.getContacts());
        this.scrollPane = new JScrollPane(table);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        this.dialog.setVisible(false);
    }

    // 초기화
    public void init() {
        this.setSize(780, 600);

        // 찾기 버튼
        searchBtn.addActionListener(e -> {
            doSearch();
        });

        // 검색어 삭제 버튼
        resetBtn.addActionListener(e -> {
            this.searchWordField.setText(""); // 검색어 삭제
            int count = this.table.setData(this.svc.getContacts()); // 저장소의 내용을 가져다가 ContactsTable(JTable)에 채운다.
            this.setStatusLabel(count);
        });

        // 추가 버튼
        addBtn.addActionListener(e -> {
            this.dialog.setData(new Contact()); // 빈 Contact 객체를 준비
            this.dialog.setVisible(true); // DetailDialog(JDialog)를 보이게 한다
        });

        // 삭제 버튼
        removeBtn.addActionListener(e -> {
            String contactId = table.getSelectedContactId();
            Contact removed = this.svc.removeContactByContactId(contactId); // 삭제
            doSearch(); // 검색을 한 상태였다면 재검색한다
            this.refreshTable(); // 테이블 새로고침
            System.out.println("contact 삭제: " + removed);
        });

        // 젤 윗쪽 패널에 추가
        this.topPanel.add(this.searchLabel);
        this.topPanel.add(this.searchCondComboBox);
        this.topPanel.add(this.searchWordField);
        this.topPanel.add(this.resetBtn);
        this.topPanel.add(this.searchBtn);
        this.topPanel.add(this.addBtn);
        this.topPanel.add(this.removeBtn);
        this.topPanel.setLayout(new FlowLayout());

        this.refreshTable();

        // 아래쪽 패널에 추가
        this.bottomPanel.add(this.statusLabel);
        this.bottomPanel.setLayout(new FlowLayout());


        this.setLayout(new BorderLayout()); /* Borderlayout은 위(PAGE_START), 아래(PAGE_END), 중간의왼쪽(LINE_START),
                                             * 중간(CENTER), 중간오른쪽(LINE_END)로 나눠진다. */
        this.add(this.topPanel, BorderLayout.PAGE_START); // 위에 패널 추가
        this.add(this.scrollPane, BorderLayout.CENTER); // 중간에 스크롤 패인 추가
        this.add(this.bottomPanel, BorderLayout.PAGE_END); // 아래에 패널 추가
        this.setVisible(true); // 이거 true로 호출 안하면 윈도우 안 보임
    }

    // index번째의 데이터를 DetailDialog(JDialog)에 전달한다음 창(DetailDialog(JDialog))을 보이게 한다.
    public void showDialog(int index) {
        this.dialog.setData(this.svc.getContacts().get(index));
        this.dialog.setVisible(true);
    }

    // ContactsTable(JTable)을 새로 고침
    public void refreshTable() {
        int count = this.table.reloadData();
        this.setStatusLabel(count);
    }

    private void setStatusLabel(int count) {
        statusLabel.setText(String.format("총 %d개의 결과를 찾았습니다.", count));
    }

    public void addContact(Contact contact) {
        this.svc.addContact(contact);
    }

    private void doSearch() {
        String searchWord = this.searchWordField.getText(); // 검색어를 가져옴
        System.out.println("검색어: " + searchWord);
        int count = 0;

        if (searchWord.equals("")) {
            count = this.table.setData(this.svc.getContacts()); // 저장소의 내용을 가져다가 ContactsTable(JTable)에 채운다.
        }
        else if (this.searchCondComboBox.getSelectedItem().toString().equals("이름")) { // 이름 검색
            count = this.table.setData(this.svc.searchContactsByName(searchWord)); // 검색 결과를 ContactsTable(JTable)에 채운다.
        }
        else if  (this.searchCondComboBox.getSelectedItem().toString().equals("전화번호")) { // 전화번호 검색
            count = this.table.setData(this.svc.searchContactsByPhoneNumber(searchWord)); // 검색 결과를 ContactsTable(JTable)에 채운다.
        }

        this.setStatusLabel(count);
    }
}
