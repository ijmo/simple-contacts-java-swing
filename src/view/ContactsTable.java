package view;

import model.Contact;
import model.Email;
import model.PhoneNumber;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class ContactsTable extends JTable {
    private final String[] header = {"이름", "전화번호", "이메일"}; // 테이블 헤더
    private MainFrame parent;
    private ArrayList<Contact> contacts;


    // 테이블 모델(테이블 컴포넌트의 실질적인 내용(헤더, 데이터)이 담기는 곳)
    private DefaultTableModel model = new DefaultTableModel(header, 0){
        public boolean isCellEditable(int row, int column)
        {
            return false;
        } // 테이블에서 수정할 수 없도록
    };


    public ContactsTable(MainFrame parentFrame, ArrayList<Contact> contacts) {

        this.parent   = parentFrame; // ContactsDialog(JDialog)를 띄우기 위해 전달받음
        this.contacts = contacts; /* 뿌려질 결과를 생성자 호출시에 미리 전달받음. (이때는 데이터를 가져오는 것이 아니라,
                                   * ArrayList<Contact>의 포인터만 가져온다. 여기서 수정하면 전달한 곳의 내용도 바뀜 */ //
        this.setModel(model);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e); // 꼭 써줘야하는지는 모르겠음
                if (e.getClickCount() == 2) { // 두번 클릭 했다면(왼쪽버튼 오른쪽버튼 상관없이. 필요하면 조건 추가하셈)
                    JTable table = (JTable) e.getSource(); // 현재 클릭된 컴포넌트(JTable)객체를 가져옴. (선택된 row가 몇번째인지 알려면 필요)
                    Point  point = e.getPoint(); // 좌표 가져옴 (선택된 row가 몇번째인지 알려면 필요)
                    int    row   = table.rowAtPoint(point); // 위에 가져온 것들을 이용해 몇번째인지 가져옴. (이런 내용은 구글링하면 나옴)
                    System.out.println("Table 더블클릭 index:" + row);
                    parent.showDialog(row); // row번째의 내용을 가지고 ContactsDialog(JDialog)를 보이게 함
                }
            }
        });
        printRowCount();
    }


    public int reloadData() {
        this.model.setRowCount(0); // delete all rows

        for (Contact c: this.contacts) { // 현재 this가 가지고 있는 contacts의
            PhoneNumber phoneNumber = c.getPhoneNumber();
            Email       email       = c.getEmail();

            String[] row = new String[header.length];

            row[0] = c.getLastName() + " " + c.getFirstName();
            row[1] = String.format("(%s)%s ", phoneNumber.getType(), phoneNumber.getPhoneNumber());
            row[2] = String.format("(%s)%s ", email.getType(), email.getEmail());

            model.addRow(row);
        }

        this.model.fireTableDataChanged(); // 이거 써줘야 화면에도 바뀌나 봄
        printRowCount();
        return model.getRowCount(); // 상태바(statusLabel)에 써먹으려고 count를 리턴
    }

    public int setData(ArrayList<Contact> contacts) {
        this.contacts = contacts; /* 전달받은 contacts를 this.contacts에 저장. (이때는 데이터를 가져오는 것이 아니라,
                                   * ArrayList<Contact>의 포인터만 가져온다. 여기서 수정하면 전달한 곳의 내용도 바뀜 */
        return reloadData();
    }

    public String getSelectedContactId() {
        int row = this.getSelectedRow(); // 선택된 행이 몇번째인지 가져옴 (0부터 시작)
        return this.contacts.get(row).getContactId();
    }

    public void printRowCount() {
        System.out.println("rowCount: " + model.getRowCount());
    }
}
