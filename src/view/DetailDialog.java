package view;

import model.Contact;
import model.Email;
import model.PhoneNumber;
import util.Validator;

import javax.swing.*;
import java.awt.*;

public class DetailDialog extends JDialog {

    private MainFrame parentFrame;

    // swing 컴포넌트들
    private JPanel[] panels = new JPanel[5]; /* DetailDialog(JDialog)를 5행*1열의 GridLayout으로 구성하고
                                              * 각 행에 JPanel을 넣는다. JPanel은 JLabel, JTextfield등의 swing component를
                                              * 가질 수 있다. (init() 참고) */
    private JLabel lastNameLabel = new JLabel("성:");
    private JTextField lastNameTextField = new JTextField(10);
    private JLabel firstNameLabel = new JLabel("이름:");
    private JTextField firstNameTextField = new JTextField(10);
    private JLabel phoneNumberLabel = new JLabel("전화번호:");
    private JComboBox phoneNumberTypeComboBox = new JComboBox(PhoneNumber.PhoneNumberTypes);
    private JTextField phoneNumberTextField = new JTextField(20);
    private JLabel emailLabel = new JLabel("이메일:");
    private JComboBox emailTypeComboBox = new JComboBox(Email.EmailTypes);
    private JTextField emailTextField = new JTextField(20);
    private JButton okBtn = new JButton("확인");
    private JButton cancelBtn = new JButton("취소");

    // 데이터
    private Contact contact; // 연락처 저장을 위한 객체
    private boolean isNew = false; // 추가인지 수정인지 구분하기 위해

    // 생성자
    public DetailDialog(MainFrame parentFrame) {
        this.parentFrame = parentFrame; // ContactsTable(JTable)에 접근해서 새로고침 하려고 MainFrame(JFrame) 객체를 전달받음

        for (int i = 0; i < panels.length; i++) {
            panels[i] = new JPanel();
            panels[i].setLayout(new FlowLayout());
        }

        init();
    }

    // 초기화
    public void init() {
        this.setSize(500, 400); // DetailDialog(JDialog)의 윈도우크기를 지정
        this.setLayout(new GridLayout(5, 1)); /* DetailDialog(JDialog)를 5행*1열으로 구성한다.
                                                          * Layout 종류는 구글링 해보면 나온다 */

        // 확인버튼을 누르면 실행할 내용
        okBtn.addActionListener(e -> { // 람다함수임

            String phoneNumber = this.phoneNumberTextField.getText();
            String email       = this.emailTextField.getText();

            if (Validator.validatePhoneNumber(phoneNumber) == false) { // 전화번호 검사
                alert("잘못된 전화번호");
                return;
            }

            if (Validator.validateEmail(email) == false) { // 이메일 검사
                alert("잘못된 이메일");
                return;
            }

            saveContact(); // Contact 객체에 데이터를 담는다
            if (isNew) {
                parentFrame.addContact(this.contact); // Service에 넘겨주기 위해 MainFrame(JFrame)에다가 Service에 저장하는 메서드를 만듦
                isNew = false;
            }
            parentFrame.refreshTable(); // 테이블 새로고침
        });

        // 취소버튼을 누르면 실행할 내용
        cancelBtn.addActionListener(e -> { // 람다함수
            this.setVisible(false);
        });

        // 각 패널에 준비된 컴포넌트를 추가한다
        panels[0].add(lastNameLabel);
        panels[0].add(lastNameTextField);
        panels[1].add(firstNameLabel);
        panels[1].add(firstNameTextField);
        panels[2].add(phoneNumberLabel);
        panels[2].add(phoneNumberTypeComboBox);
        panels[2].add(phoneNumberTextField);
        panels[3].add(emailLabel);
        panels[3].add(emailTypeComboBox);
        panels[3].add(emailTextField);
        panels[4].add(okBtn);
        panels[4].add(cancelBtn);

        for (int i = 0; i < panels.length; i++) {
            this.add(panels[i]); // this - 이하 DetailDialog(JDialog) - 에 준비된 패널을 추가한다
        }
    }

    // ComboBox, TextField 초기화
    public void setData() {
        lastNameTextField.setText(""); // 빈칸으로
        firstNameTextField.setText("");
        phoneNumberTypeComboBox.setSelectedItem(0); // 첫번째 아이템으로
        phoneNumberTextField.setText("");
        emailTypeComboBox.setSelectedItem(0);
        emailTextField.setText("");
    }

    // Contact에서 가져오기
    public void setData(Contact contact) {
        this.contact = contact;

        if (contact.isEmpty()) { // 빈 Contact 일 때, '추가' 모드 설정
            this.isNew = true;
            return;
        }

        PhoneNumber phoneNumber = contact.getPhoneNumber();
        Email email = contact.getEmail();
        lastNameTextField.setText(contact.getLastName());
        firstNameTextField.setText(contact.getFirstName());
        phoneNumberTypeComboBox.setSelectedItem(phoneNumber.getType());
        phoneNumberTextField.setText(phoneNumber.getPhoneNumber());
        emailTypeComboBox.setSelectedItem(email.getType());
        emailTextField.setText(email.getEmail());
    }

    // 저장하기
    public void saveContact() {
        // swing component에서 데이터를 가져온다
        String lastName        = lastNameTextField.getText();
        String firstName       = firstNameTextField.getText();
        String phoneNumberType = phoneNumberTypeComboBox.getSelectedItem().toString();
        String phoneNumber     = phoneNumberTextField.getText();
        String emailType       = emailTypeComboBox.getSelectedItem().toString();
        String email           = emailTextField.getText();

        // this.contact에 저장한다
        this.contact.setLastName(lastName);
        this.contact.setFirstName(firstName);
        this.contact.setPhoneNumber(new PhoneNumber(phoneNumberType, phoneNumber));
        this.contact.setEmail(new Email(emailType, email));

        this.setVisible(false);
    }

    // 경고창 띄우기
    private void alert(String msg) {
        JOptionPane optionPane = new JOptionPane(msg,JOptionPane.WARNING_MESSAGE);
        JDialog dialog = optionPane.createDialog("Warning!");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }
}
