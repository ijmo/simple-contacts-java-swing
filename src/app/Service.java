package app;

import model.Contact;

import java.util.ArrayList;

public class Service {

    // 연락처 저장소
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // getter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // 연락처 1건 추가
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    // 이름에서 찾기
    public ArrayList<Contact> searchContactsByName(String name) {
        String nameLowerCase = name.toLowerCase(); // 찾을 문자열을 소문자로 변환
        ArrayList<Contact> result = new ArrayList<Contact>();

        for (Contact c: this.contacts) { // 모든 연락처 저장소 순회
            // 연락처의 성이름을 소문자로 변환
            String fullName = c.getLastName().toLowerCase() + c.getFirstName().toLowerCase();

            if (fullName.toLowerCase().indexOf(nameLowerCase) > -1) { // fullName에 nameLowerCase가 있다면 위치 반환, 없으면 -1
                result.add(c);
                System.out.println("found: " + c.toString());
            }
        }
        return result;
    }

    // 전화번호 찾기
    public ArrayList<Contact> searchContactsByPhoneNumber(String phoneNumber) {
        String searchingPhoneNumber = phoneNumber.replace("-", "");; // - 제거
        ArrayList<Contact> result = new ArrayList<Contact>();

        for (Contact c: this.contacts) { // 모든 연락처 저장소 순회
            String aPhoneNumber = c.getPhoneNumber().getPhoneNumber().replace("-", ""); // - 제거

            if (aPhoneNumber.indexOf(searchingPhoneNumber) > -1) { // aPhoneNumber에 searchingPhoneNumber가 있다면 위치 반환, 없으면 -1
                result.add(c);
                System.out.println("found: " + c.toString());
            }
        }
        return result;
    }

    private int searchIndexByContactId(String contactId) {
        // 트랜잭션 처리가 필요하지만 생략
        for (int i = 0; i < this.contacts.size(); i++) {
            if (contactId.equals(this.contacts.get(i).getContactId())) {
                return i;
            }
        }
        return -1;
    }

    public Contact removeContactByContactId(String contactId) {
        // 트랜잭션 처리가 필요하지만 생략
        int idx = searchIndexByContactId(contactId);

        if (idx == -1) { // 해당 contactId로 못 찾음
            return null;
        }

        return this.contacts.remove(idx);
    }
}
