package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validator {
    // 전화번호 검사
    public static boolean validatePhoneNumber(String phoneNumber) {
        String pattern = "[0-9\\-]+"; // 숫자 또는 -로 이루어짐
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(phoneNumber);

        if (m.matches()) {
            return true;
        }
        else {
            return false;
        }
    }

    // 이메일 검사
    public static boolean validateEmail(String phoneNumber) {
        String pattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"; // 숫자 또는 -로 이루어짐
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(phoneNumber);

        if (m.matches()) {
            return true;
        }
        else {
            return false;
        }
    }
}
