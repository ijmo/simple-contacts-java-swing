package model;

import util.Validator;

public class Email {
    public static final String[] EmailTypes = {"집", "직장"};
    private String type;
    private String email;

    public Email(String type, String email) {
        this.type = type;
        setEmail(email);
    }

    public String getType() {
        return type;
    }

    public int getTypeIndex(String emailType) { // 전달받은 emailType가 EmailTypes에 있는지 확인. (하려고 짰는데 지금 필요없는듯)
        for (int i = 0; i < EmailTypes.length; i++) {
            if (EmailTypes[i].equals(emailType)) {
                return i;
            }
        }
        return -1;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (Validator.validateEmail(email)) { // 이메일 형식이 올바른지 확인
            this.email = email;
        }
        else {
            throw new IllegalArgumentException("잘못된 이메일");
        }
    }
}
