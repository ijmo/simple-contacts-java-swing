package model;

import util.Generator;

public class Contact {
    private String contactId;
    private String lastName;
    private String firstName;
    private PhoneNumber phoneNumber;
    private Email email;

    public Contact() {
        this.contactId   = Generator.generateId();
        this.lastName    = null;
        this.firstName   = null;
        this.phoneNumber = null;
        this.email       = null;
    }

    public Contact(String lastName, String firstName,
                   String phoneNumberType, String phoneNumber,
                   String emailType, String email) {
        this.contactId   = Generator.generateId();
        this.lastName    = lastName;
        this.firstName   = firstName;
        this.phoneNumber = new PhoneNumber(phoneNumberType, phoneNumber);
        this.email       = new Email(emailType, email);
    }

    public String getContactId() {
        return contactId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public String toString() { // Object 클래스의 toString()함수 오버라이드(덮어쓰기). (디버그에 쓰려고 일부러 만듦)
        String result = String.format("%s %s, (%s)%s, (%s)%s", lastName, firstName,
                phoneNumber.getType(), phoneNumber.getPhoneNumber(),
                email.getType(), email.getEmail());
        return result;
    }

    public boolean isEmpty() {

        if (lastName == null && firstName == null && phoneNumber == null && email == null) {
            return true;
        }

        return false;
    }
}
