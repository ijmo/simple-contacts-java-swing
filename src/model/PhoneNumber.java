package model;

import util.Validator;

public class PhoneNumber {
    public static final String[] PhoneNumberTypes = {"휴대전화", "집", "직장"};
    private String type;
    private String phoneNumber;


    public PhoneNumber(String type, String phoneNumber) {
        this.type = type;
        setPhoneNumber(phoneNumber);
    }

    public String getType() {
        return type;
    }

    public int getTypeIndex(String phoneNumberType) { // 전달받은 phoneNumberType PhoneNumberTypes 있는지 확인. (하려고 짰는데 지금 필요없는듯)
        for (int i = 0; i < PhoneNumberTypes.length; i++) {
            if (PhoneNumberTypes[i].equals(phoneNumberType)) {
                return i;
            }
        }
        return -1;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if (Validator.validatePhoneNumber(phoneNumber)) { // 전화번호 형식이 올바른지 확인
            this.phoneNumber = phoneNumber;
        }
        else {
            throw new IllegalArgumentException("잘못된 전화번호");
        }
    }
}
